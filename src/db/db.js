const sqlInstance = require('mssql');
const configFile = require('../config/dbconfig');

//connect to your database
const poolPromise = new sqlInstance.ConnectionPool(configFile)
.connect()
.then(pool => {
    console.log('Connected to MSSQL')
    return pool
})
.catch(err => console.log('Database Connection Failed! Bad Config: ', err))

module.exports = poolPromise;