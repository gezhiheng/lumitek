const Router = require('koa-router')   //路由模块
const baccount = require('../controller/baccount')
const router = new Router()

router.get('/index', async (context, next) => {
    context.body = {
      data: { get: 1 },
      status: 0
    }
})

//获取用户列表
router.get('/baccountlist', async (context, next) => {
  let baccountList = await baccount.findBaccount(5)
  console.log(baccountList);
  context.body = {
    data: baccountList
  }
})

module.exports = router