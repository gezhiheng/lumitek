const poolPromise = require('../db/db')

module.exports = {
  //新增用户
  async addUser (name,age) {
      let res = await SQLQuery(`insert into users (name,age) values ('${name}','${age}')`);
      return res
  },
  //获取用户列表
  async findBaccount (num) {
    const pool = await poolPromise;
    console.log('Connected to MSSQL');
    const queryResult = await new Promise((resolve, reject) => {
        pool.query(`select top ${num} * from baccount`, function (err, recordset) {
          if (err) {
            reject(err)
          }
          resolve(recordset)
        });
    });
    return queryResult;
  },
  //更新用户列表
  async updateUserList(name,age) {
    let res = await SQLQuery(`update users set age = ${age} where name = '${name}'`)
    return res
  },
  //删除用户
  async delUser(id) {
    let res = await SQLQuery(`delete from users where id = ${id}`)
    return res
  }
}
