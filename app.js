const Koa = require('koa')
const app = new Koa()
const JSON_MIME = 'application/json'    //json类型
const router = require('./src/route/route')  //路由
const bodyParser = require('koa-bodyparser')
const { koaBody } = require('koa-body')

app.use(koaBody({
  multipart: true,  // 支持表单上传
  formidable: {
    maxFileSize: 10 * 1024 * 1024, // 修改文件大小限制，默认位2M
  }
}))

app.use(async (context, next) => {
  context.type = JSON_MIME
  await next()
})

app.use(bodyParser())
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(8010)
console.log('app start at 8010')
